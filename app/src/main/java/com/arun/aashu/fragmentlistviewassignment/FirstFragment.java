package com.arun.aashu.fragmentlistviewassignment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    View vie;
    ListView listview;
    String items[] = {"Android 1.1","Cup cake","Donut","Eclair","Froyo","Ginger Bread","Honey Comb"
    ,"Ice  Cream Sand wich","Jelly Bean","Kitkat","Lollipop","Marshmallow","Nougat","Oreo"};

    int[] img={R.drawable.androidfirst,R.drawable.cupcake,R.drawable.donut,R.drawable.eclair,R.drawable.froyo,R.drawable.gingerbread,
            R.drawable.honeycomb,R.drawable.icecreamsandwich,R.drawable.jellybean,R.drawable.kitkat,R.drawable.lollipop,
    R.drawable.marshmallow,R.drawable.nougat,R.drawable.oreos};


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        vie=inflater.inflate(R.layout.fragment_first, container, false);
         listview =vie.findViewById(R.id.listview1);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                FragmentManager fm=getFragmentManager();

                SecondFragment fra=new SecondFragment();
                SecondFragment secondFragment= (SecondFragment) fm.findFragmentById(R.id.fragment2);

                secondFragment.Jadu(img[i]);
                secondFragment.TextValue(items[i]);






            }
        });


        return vie;
    }

}

