package com.arun.aashu.fragmentlistviewassignment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {
    View vi;
    ImageView imageView;
    String t;
    TextView tv;

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vi=inflater.inflate(R.layout.fragment_second, container, false);
        imageView=vi.findViewById(R.id.imgview);
         tv=vi.findViewById(R.id.tv1);



        return vi;
    }

    public void Jadu(Integer bmp){


        imageView.setImageResource(bmp);
    }
    public void TextValue(String msg){

        tv.setText(msg);

    }



}
